// require() and import both imports your modules

import React,{useState,useEffect} from 'react';
// import Banner from './components/Banner'
import AppNavBar from './components/AppNavBar'
// import Course from './components/Course'
// import Highlights from './components/Highlights'

import { BrowserRouter as Router } from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';

import {UserProvider} from './userContext'

// import pages
import Home from './pages/Home'
import Courses from './pages/Courses'
import ViewCourse from './pages/ViewCourse'
import Register from './pages/Register'
import Login from './pages/Login'
import NotFound from './pages/NotFound'
import Logout from './pages/Logout'
import AddCourse from './pages/AddCourse'

import {Container} from 'react-bootstrap'

// import app css in this component
import './App.css'

export default function App() {


	const [user,setUser] = useState({
		id: null,
		isAdmin: null
	})

	useEffect(()=>{
		fetch('http://localhost:4000/users/getUserDetcails',{

			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	},[])

	console.log(user)

	// ReactJS is a single page application (SPA)


	// function to clear localStorage on logout
	const unsetUser = () => {

		localStorage.clear()
	}


	return (
		<>
			<UserProvider value={{user,setUser, unsetUser}}>
				<Router>
					<AppNavBar/>
						<Container>
							<Switch>
								<Route exact path="/" component={Home}/>
								<Route exact path="/courses" component={Courses}/>
								<Route exact path="/courses/:courseId" component={ViewCourse}/>
								<Route exact path="/login" component={Login}/>
								<Route exact path="/logout" component={Logout}/>
								<Route exact path="/register" component={Register}/>
								<Route exact path="/addCourse" component={AddCourse}/>
								<Route component={NotFound}/>
							</Switch>
						</Container>
				</Router>
			</UserProvider>	
		</>
	)

}