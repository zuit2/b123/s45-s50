import React,{useContext} from 'react'
import {Navbar, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../userContext'

export default function AppNavBar() {

	const {user} = useContext(UserContext);
	console.log(user);
	
	// if component has children, it should have a closing tag. If not, then self-close tag
	// classes in reactjs are added as className instead of Class.


	return (
		
			<Navbar bg="primary" expand="lg">
				<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="m1-auto">
						<Nav.Link as={Link} to="/">Home</Nav.Link>
						<Nav.Link as={Link} to="/courses">Courses</Nav.Link>
						{
							user.id
							?
								user.isAdmin
								?
								<>
								<Nav.Link as={Link} to="/addCourse">Add Course</Nav.Link>
								<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
								</>
								:
								<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							: 
							<>
								<Nav.Link as={Link} to="/register">Register</Nav.Link>
								<Nav.Link as={Link} to="/login">Login</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Navbar>

		)

}