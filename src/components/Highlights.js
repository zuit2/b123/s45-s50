import React from 'react';
import {Row,Col,Card} from 'react-bootstrap'

export default function Highlights(){

	return (

			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Est aliqua aliqua non enim ad ut aute excepteur mollit esse labore duis aliqua dolore cillum sit
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Est aliqua aliqua non enim ad ut aute excepteur mollit esse labore duis aliqua dolore cillum sit occaecat labore voluptate exercitation cupidatat nisi velit ea do ad mollit ut aute 
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Est aliqua aliqua non enim ad ut aute excepteur mollit esse labore duis aliqua dolore cillum sit occaecat labore voluptate exercitation cupidatat nisi velit ea do ad mollit ut aute dolor exercitation ad dolore dolor aliqua velit proident fugiat nisi cupidatat elit quis ut consectetur ullamco velit est excepteur minim quis minim ex et nisi ad dolore sint commodo adipisicing adipisicing cillum nostrud amet nisi elit tempor do officia id excepteur reprehenderit voluptate
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>

		)

}