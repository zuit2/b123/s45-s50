import React from 'react';
import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Course({courseProp}) {

	console.log(courseProp);

	
	// States in Reactjs are ways to store information within a compondent.
	/*const [count,setCount] = useState(0);
	const [seats,setSeats] = useState(30);
	const [isActive,setIsActive] = useState(true);*/

	// useState is a react hook which allows us to create a state and its setter function. useState actually returns 2 items in an array, with the argument in it becoming the initial value of our state variable.

	// Conditional Rendering is when we are able to show/hide content based on a condition

	// console.log(useState(0));

	// This console log repeats whenever the button is presssed
	// console.log(courseProp);

	/*useEffect(()=>{
		if(seats === 0){
			setIsActive(false);
		}
	},[seats]);


	console.log(isActive);*/


/*
	function enroll (){
		// alert("Hello")
		setCount(count + 1);
		setSeats(seats - 1);
		// varCount++;
		// console.log(varCount)
	}*/

	/*
		If enroll() by clicking button, setSeats(count - 1)
			<Card.Text>seats
				if seats === 0, red text "No More Seats Available"
				else green text <no. of seats>

		if <no. of seats> === 0, show disabled button
		else show working button

	*/

	return (

		<Card>
			<Card.Body>
				<Card.Title>{courseProp.name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{courseProp.description}:</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{courseProp.price}:</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${courseProp._id}`}>View Course</Link>
			</Card.Body>
		</Card>



		/*<Card className="p-3">
			<Card.Title>
				<h3>{courseProp.name}</h3>
			</Card.Title>
			<Card.Body>
				<Card.Text>{courseProp.description}</Card.Text>
				<Card.Text>PHP {courseProp.price}</Card.Text>
				<Card.Text>Enrollees: {
					count === 0 
					? <span className="text-danger">No Enrollees.</span>
					: <span className="text-success">{count}</span>
				}
				</Card.Text>
				<Card.Text>Seats: {
					seats === 0 
					? <span className="text-danger">No More Seats Available.</span>
					: <span className="text-success" >{seats}</span>
				}
				</Card.Text>
			</Card.Body> 
			{
				isActive === false
				? <Button variant="primary" disabled={true}>Enroll</Button>
				: <Button variant="primary" onClick={enroll}>Enroll</Button>
			}	
		</Card>*/
		)

}