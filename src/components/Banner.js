import React from 'react';
import {Row,Col,Jumbotron} from 'react-bootstrap';

import {Link} from 'react-router-dom';

/*

	react-bootstrap components create
*/

export default function Banner({bannerProp}){

	console.log(bannerProp);

	return (

			<Row>
				<Col>
					<Jumbotron>
						<h1>{bannerProp.title}</h1>
						<p>{bannerProp.description}</p>
						<Link to={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonCallToAction}</Link>
					</Jumbotron>
				</Col>
			</Row>

		)

}