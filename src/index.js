import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// import bootstrap in index.js, our entry script:
import 'bootstrap/dist/css/bootstrap.min.css'

// let element = <h1>My First React App!</h1>
// console.log(element);

// with JSX
// let myName = <h2>Efren Rodriguez</h2>

// without JSX
// let myName = React.createElement('h2',{},"This was not created with JSX Syntax");

/*let person = {

  name: "Stephen Strange",
  age: 45,
  job: "Sorcerer Supreme",
  income: 50000,
  expense: 30000
}


let sorcererSupreme = ( 

  <>
    <p>My name {person.name}. I am {person.age} old. I work as {person.job}</p>
    <p>I am {person.name}. I work as {person.job}. My income is {person.income}. My expense is {person.expense}. My balance is {person.income-person.expense}.</p>
  </>
)*/

// Components are functions that return react elements.

// Components naming convention: PascalCase

// Componenet should start in capital letters.

/*const SampleComp = () => {

  return (

      <h1>I am returned by a function.</h1>
  )

}*/

// How are components called?
// Create a self-closing tag <SampleComp />

/*
  
  In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component.

  All other components/pages will be contained in our main component: <App />
    
    React.StrictMode is built in react component which highlights potential problems in the code and provide more error information.
*/

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root'));



