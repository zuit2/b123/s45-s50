import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

/*

	Home will be a page component, which will be our pages for the application

	ReactJS adheres to D.R.Y. - Don't Repeat Yourself.

	Props - data we can pass from a parent component to child component

*/
export default function Home(){

	let sampleProp = {
		title: "Peach Booking System",
		description: "Moving Filipinos forward!",
		buttonCallToAction: "Start now!",
		destination: "/register"
	}

	/*
		we can pass props from parent to child by adding HTML-like attribute which we can name ourselves. The name of the attribute will become the property name of the object received by all components.
	*/

	return (

			<>
				<Banner bannerProp={sampleProp}/>
				<Highlights />
			</>

		)


}