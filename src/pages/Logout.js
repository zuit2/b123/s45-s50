import React,{useContext,useEffect} from 'react';
import UserContext from '../userContext'
import Banner from '../components/Banner'

export default function Logout(){

	const {setUser,unsetUser} = useContext(UserContext);
	// console.log(setUser);
	// console.log(unsetUser);

	// clear the localStorage
	unsetUser();

	useEffect(()=>{

		// set the global user state to its initial values.
		setUser({
				id: null,
				isAdmin: null
		})
	},[])


	const bannerContent = {
		title: "See You Later!",
		description: "You have logged out of Peach Booking System",
		buttonCallToAction: "Go Back to Home Page.",
		destination: "/"
	}

	return (

		<>
			<Banner bannerProp={bannerContent}/>
		</>

		)
}