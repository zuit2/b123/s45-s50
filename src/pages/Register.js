import React,{useState,useEffect,useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../userContext'

import {Redirect,useHistory} from 'react-router-dom'

export default function Register (){

	const {user} = useContext(UserContext);
	console.log(user);

	const history = useHistory();

	/*

	Hooks are special/react-defined methods and functions that allow us to do certain tasks in our components.

	useState() is a hook that creates states. useState() returns an array with 2 items. The first item in the array is state and 2nd is setter function. we then destructure this returned array and assign both items in variables:

		const [stateName,setStateName] = useState(intial value of State)

	useEffect() hook is used to create effets. These effects will allow us to run a function or task based on when our effect will run. Our useEffect will ALWAYS run on initial render, This next time that the useEffect will run will depend on its dependency array.

	useEffect(()=>{},[dependency array])
*/

	/*const [state1, setState1] = useState(0);
	const [state2, setState2] = useState(0);

	function sample1(){
		setState1(state1 + 1);
	}

	function sample2(){
		setState2(state2 + 1);
	}

	useEffect(()=>{

		console.log("I will run only when state 2 is updated.")

	},[state2])

	return (

		<>
			<Button variant="success" className="mx-3 my-5" onClick={sample1}>Update State 1</Button>
			<Button variant="danger" className="mx-3 my-5" onClick={sample2}>Update State 2</Button>
		</>
	)*/

	// input states
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")

	// conditional rendering for button
	const [isActive,setIsActive] = useState(false)

	/* 
		Two Way Binding
		
		save value of input. capture current value

		<Form.Control type="InputType" value={inputState} onChange={e => {setInputState(e.target.value)}}/>

		e = event, all event listeners pass the even objedt to the function added in event listener

		e.target = target is the elemen where the even happened.

		e.target.value =  value is the parent value of the element where the event happened.


	*/

	/*console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password);
	console.log(confirmPassword);*/
	console.log(isActive);

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "")&&(password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[firstName,lastName,email,mobileNo,password,confirmPassword])


	function registerUser(e){

		// prevent submit's default behavior
		e.preventDefault();

		/*console.log(firstName);
		console.log(lastName);
		console.log(email);
		console.log(mobileNo);
		console.log(password);
		console.log(confirmPassword);*/

		fetch('http://localhost:4000/users/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data.email){
				Swal.fire({
					Icon: "success",
					title: "Registration Successful!",
					text: `Thank you registering, ${data.email}`
				})

				history.push('/login')
				 
			} else {
				Swal.fire({
					Icon: "error",
					title: "Registration Failed.",
					text: data.message
				})
			}
		})
	}

	return (
		user.id
		?
		<Redirect to="/courses"/>
		:
		<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" value={firstName} onChange={e => {setFirstName(e.target.value)}} placeholder="Enter First Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" value={lastName} onChange={e => {setLastName(e.target.value)}} placeholder="Enter Last Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" value={email} onChange={e => {setEmail(e.target.value)}} placeholder="Enter Email" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} placeholder="Enter 11 Digit Mobile No" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}}  placeholder="Enter Password" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}  placeholder="Confirm Password" required/>
				</Form.Group>
				{
					isActive
					? <Button variant="primary" type="submit" >Submit</Button>
					: <Button variant="primary" disabled>Submit</Button>
				}
			</Form>
		</>
		)

}