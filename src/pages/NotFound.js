import React from 'react';
import Banner from '../components/Banner'

export default function NotFound() {

	let notFoundProp = {
		title: "Page Cannot Be Found",
		description: "Moving Filipinos forward!",
		buttonCallToAction: "Back to Home",
		destination: "/"
	}

	return (

			<>
				<Banner bannerProp={notFoundProp}/>
			</>

		)

}