import React,{useState,useEffect} from 'react';
// import coursesData from '../data/coursesData';
import Course from '../components/Course';

export default function Courses(){

	// create state with empty array as initial value
	const [coursesArray,setCoursesArray] = useState([]);

	// console.log(courseData);

	useEffect(()=>{

		fetch("http://localhost:4000/courses/getActiveCourses")
		.then(res=>res.json())
		.then(data=> {

			setCoursesArray(data.map(course => {

				return (
						
						<Course key={course._id} courseProp={course}/>
					)


		/*if(course.onOffer){
			return (
				// when creating array, key prop is needed
				<Course key={course.id} courseProp={course}/>
			)

		} else {
			return null
		}*/

		
			}))
		})

	},[])

	// console.log(coursesArray);

	// let sampleProp1 = "I am sample 1";
	// let sampleProp2 = "I am sample 2";

	// let courseCards = coursesData

	// console.log(courseCards);
	

	return (
		<>
			<h1 className="my-5">Available Courses</h1>
			{coursesArray}
		</>
		)
}