let coursesData = [
	
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Laborum aliquip velit do culpa laboris velit occaecat excepteur quis anim excepteur consectetur consequat cillum sed fugiat officia dolor aliquip consectetur reprehenderit proident aute ut eu sunt reprehenderit nostrud consectetur ut id consectetur ullamco incididunt labore duis dolor pariatur incididunt laboris enim incididunt commodo enim dolor est culpa irure reprehenderit sint aliqua mollit officia qui qui duis deserunt culpa officia  dolore officia mollit elit cupidatat veniam reprehenderit sit cupidatat qui adipisicing culpa ea sed.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Phython-Django",
		description: "Laborum aliquip velit do culpa laboris velit occaecat excepteur quis anim excepteur consectetur consequat cillum sed fugiat officia dolor aliquip consectetur reprehenderit proident aute ut eu sunt reprehenderit nostrud consectetur ut id consectetur ullamco incididunt labore duis dolor pariatur incididunt laboris enim incididunt commodo enim dolor est culpa irure reprehenderit sint aliqua mollit officia qui qui duis deserunt culpa officia  dolore officia mollit elit cupidatat veniam reprehenderit sit cupidatat qui adipisicing culpa ea sed.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc00wr3",
		name: "PHP-Laravel",
		description: "Laborum aliquip velit do culpa laboris velit occaecat excepteur quis anim excepteur consectetur consequat cillum sed fugiat officia dolor aliquip consectetur reprehenderit proident aute ut eu sunt reprehenderit nostrud consectetur ut id consectetur ullamco incididunt labore duis dolor pariatur incididunt laboris enim incididunt commodo enim dolor est culpa irure reprehenderit sint aliqua mollit officia qui qui duis deserunt culpa officia  dolore officia mollit elit cupidatat veniam reprehenderit sit cupidatat qui adipisicing culpa ea sed.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc00wr4",
		name: "Nodejs-MERN",
		description: "Laborum aliquip velit do culpa laboris velit occaecat excepteur quis anim excepteur consectetur consequat cillum sed fugiat officia dolor aliquip consectetur reprehenderit proident aute ut eu sunt reprehenderit nostrud consectetur ut id consectetur ullamco incididunt labore duis dolor pariatur incididunt laboris enim incididunt commodo enim dolor est culpa irure reprehenderit sint aliqua mollit officia qui qui duis deserunt culpa officia  dolore officia mollit elit cupidatat veniam reprehenderit sit cupidatat qui adipisicing culpa ea sed.",
		price: 45000,
		onOffer: false
	},
	
]

export default coursesData;